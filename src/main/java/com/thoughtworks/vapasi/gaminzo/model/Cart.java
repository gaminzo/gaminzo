package com.thoughtworks.vapasi.gaminzo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cart {

    private String userName;

    private List<CartItemDto> cartItemDtos;

    private long total;

    public void addCartItem(CartItemDto item)
    {
        if(cartItemDtos==null)
            cartItemDtos=new ArrayList<>();
        cartItemDtos.add(item);
    }

    private long computeTotal()
    {
        long total=0;
        //TODO : Streaming
       for(CartItemDto item: getCartItemDtos())
            total=total+ item.getCount()*item.getPrice();
       this.total=total;
       return this.total;

    }

    public long getTotal()
    {
        return computeTotal();
    }
}
