package com.thoughtworks.vapasi.gaminzo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long userId;
    private long productId;
    private long count;

    public CartItem(long userId, long productId, long count) {
        this.userId = userId;
        this.productId = productId;
        this.count = count;
    }
}
