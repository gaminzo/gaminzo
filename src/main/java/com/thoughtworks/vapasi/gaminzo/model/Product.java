package com.thoughtworks.vapasi.gaminzo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    private String name;
    private long quantity;
    private long price;
    private String imageUrl;
    @Column(length = 1337)
    private String description;

    @CreatedDate
    private Date creationDate=new Date();

    public Product(String name, long quanitity, long price, String imageUrl, String description) {
        this.name = name;
        this.quantity = quanitity;
        this.price = price;
        this.imageUrl = imageUrl;
        this.description = description;
        this.creationDate = new Date();
    }

}
