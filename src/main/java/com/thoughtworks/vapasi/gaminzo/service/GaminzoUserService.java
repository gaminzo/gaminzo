package com.thoughtworks.vapasi.gaminzo.service;

import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import com.thoughtworks.vapasi.gaminzo.repository.GaminzoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class GaminzoUserService {

    @Autowired
    GaminzoUserRepository gaminzoUserRepository;

    public GaminzoUser findByUsername(String username)
    {
       return gaminzoUserRepository.findByUsername(username);

    }

    public GaminzoUser saveGaminzoUser(GaminzoUser gaminzoUser)
    {
       return gaminzoUserRepository.save(gaminzoUser);
    }


    public long fetchUserIdFromName()
    {
        GaminzoUser gaminzoUser=findByUsername(fetchUserDetails().getUsername());
        return gaminzoUser.getId();
    }

    public UserDetails fetchUserDetails()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user=(UserDetails) authentication.getPrincipal();
        return user;
    }

}
