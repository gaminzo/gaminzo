package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CartItemRepository extends JpaRepository<CartItem,Long> {

    List<CartItem> findByUserId(long userId);

    Optional<CartItem> findByUserIdAndProductId(long userId, long productId);

    default CartItem saveCartItem(CartItem entity)
    {
        Optional<CartItem> cartItem = findByUserIdAndProductId(entity.getUserId(),entity.getProductId());
        if(cartItem.isPresent())
        {
            CartItem item=cartItem.get();
            item.setCount(item.getCount()+1);
            return save(item);
        }
        return save(entity);
    }

    default void deleteAllByUserId(long userId)
    {
        List<CartItem> items = findByUserId(userId);
        deleteAll(items);
    }
}
