package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import com.thoughtworks.vapasi.gaminzo.model.RegisterUserDto;
import com.thoughtworks.vapasi.gaminzo.service.GaminzoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/gaminzo")
public class GaminzoUserController {

    @Autowired
    GaminzoUserService gaminzoUserService;

    @Autowired
    PasswordEncoder encoder;

    @GetMapping(value = "/login")
    public String login(Model model)
    {
        model.addAttribute("user",new RegisterUserDto());
        return "login";
    }

    @GetMapping(value = "/signup")
    public String signup(Model model)
    {
        model.addAttribute("user",new RegisterUserDto());
        return "signup";
    }

    @GetMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response)
    {
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
        if(authentication!=null)
            new SecurityContextLogoutHandler().logout(request,response,authentication);
        return "redirect:/login";
    }

    @PostMapping(value = "/signup")
    public String signup(@Valid @ModelAttribute("user")RegisterUserDto user, BindingResult result, Model model)
    {
        if(result.hasErrors())
        {
            return "signup";
        }

        GaminzoUser gaminzoUser=new GaminzoUser(user.getUsername(),user.getPassword(),user.getRole());
        String encryptedPassword=encoder.encode(gaminzoUser.getPassword());
        gaminzoUser.setPassword(encryptedPassword);
        GaminzoUser savedUser=gaminzoUserService.saveGaminzoUser(gaminzoUser);
        model.addAttribute("user",savedUser);
        return "redirect:signup?success";

    }
}
